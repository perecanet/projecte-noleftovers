<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestaurantsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CistellaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [IndexController::class, 'index']);

Route::get('restaurants', [RestaurantsController::class, 'list']);
Route::post('restaurants', [RestaurantsController::class, 'listSearch']);
Route::get('restaurant', [ProductsController::class, 'list']);
Route::post('restaurant', [ProductsController::class, 'filter']);
Route::post('/restaurant/addToChart', 'ProductsController@addToChart');
Route::post('/restaurant/clearChart', 'ProductsController@borrarChart');
Route::post('restaurantsP', [RestaurantsController::class, 'listSearch']);
Route::get('restaurants/descomptes', [RestaurantsController::class, 'listDescomptes']);
Route::get('restaurants/menus', [RestaurantsController::class, 'listMenus']);
Route::get('restaurants/lotes', [RestaurantsController::class, 'listLotes']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('restaurants/tipus', [RestaurantsController::class,'listTipus']);


Route::get('cistella', [CistellaController::class, 'show']);
Route::get('cistella/borrar', [CistellaController::class, 'deletefromChart']);

Route::get('nouproducte', [ProductsController::class,'addProduct']);
Route::post('nouproducte', [ProductsController::class,'storeProduct'])->name('product.store');




