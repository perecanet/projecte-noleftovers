<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RestaurantsSeeder::class);
        $this->call(ProductsSeeder::class);
    }
}

class ProductsSeeder extends Seeder
{
    public function run()
    {
        DB::table('products')->insert([
            'name'=>'Pizza Margarita',
            'restaurantID'=>6,
            'price'=> 10.50,
            'description' => 'Pizza amb salsa de tomatic natural, formatge parmesa i orenga.',
            'image'=> 'pizza1.jpg',
            'category'=>'Pizza',
        ]);

        DB::table('products')->insert([
            'name'=>'Pizza Carbonara',
            'restaurantID'=>6,
            'price'=> 11.50,
            'description' => 'Pizza amb salsa de tomatic natural, formatge parmesa, pernil dolç, champinyons i orenga.',
            'image'=> 'pizza2.jpg',
            'category'=>'Pizza',
        ]);

        DB::table('products')->insert([
            'name'=>'Pizza Funghi',
            'restaurantID'=>6,
            'price'=> 12,
            'description' => 'Pizza amb salsa de tomatic natural, formatge parmesa, espinaques, champinyons i orenga.',
            'image'=> 'pizza3.jpg',
            'category'=>'Pizza',
        ]);

        DB::table('products')->insert([
            'name'=>'Amanida Cesar',
            'restaurantID'=>6,
            'price'=> 6.50,
            'description' => 'Amanida amb pollastre, ceba, lletuga, tomatics cherry, ...',
            'image'=> 'amanida1.jpg',
            'category'=>'Amanida',
        ]);

        DB::table('products')->insert([
            'name'=>'Amanida De La Casa',
            'restaurantID'=>6,
            'price'=> 6,
            'description' => 'Amanida amb lletufa, olives, tomatic, ou bullit, ...',
            'image'=> 'amanida2.jpg',
            'category'=>'Amanida',
        ]);

        DB::table('products')->insert([
            'name'=>'Menu individual: Pizza + Beguda + Postre',
            'restaurantID'=>6,
            'price'=> 15.90,
            'description' => "Menu d'una pizza margadita amb 1 postre i beguda a elecció",
            'image'=> 'menu1.jpg',
            'category'=>'Menu',
        ]);

        DB::table('products')->insert([
            'name'=>'Menu parelles: 2 Pizzes + Beguda Gran',
            'restaurantID'=>6,
            'price'=> 25.50,
            'description' => "Menu de dues pizzes a eleccio amb 1 beguda gran a elecció",
            'image'=> 'menu2.jpg',
            'category'=>'Menu',
        ]);

        DB::table('products')->insert([
            'name'=>'Pizzes margarita',
            'restaurantID'=>6,
            'price'=> 5.50,
            'description' => 'Pizza amb salsa de tomatic natural, formatge parmesa i orenga.',
            'image'=> 'lot1.jpg',
            'category'=>'Lot',
        ]);

        DB::table('products')->insert([
            'name'=>'Pizzes Carbonara',
            'restaurantID'=>6,
            'price'=> 6,
            'description' => 'Pizza amb salsa de tomatic natural, formatge parmesa, pernil dolç, champinyons i orenga.',
            'image'=> 'lot2.jpg',
            'category'=>'Lot',
        ]);

        DB::table('products')->insert([
            'name'=>'Coca-cola',
            'restaurantID'=>6,
            'price'=> 2,
            'description' => 'Coca-cola de 33 cl',
            'image'=> 'beguda1.jpg',
            'category'=>'Beguda',
        ]);

        DB::table('products')->insert([
            'name'=>'Fanta de taronja',
            'restaurantID'=>6,
            'price'=> 2,
            'description' => 'Fanta de tronja de 33 cl',
            'image'=> 'beguda2.jpg',
            'category'=>'Beguda',
        ]);

        DB::table('products')->insert([
            'name'=>'Coulant de xocolata',
            'restaurantID'=>6,
            'price'=> 3.5,
            'description' => 'Coulnt caser de xocolata',
            'image'=> 'postre1.jpg',
            'category'=>'Postre',
        ]);

        DB::table('products')->insert([
            'name'=>'Pastis de formatge',
            'restaurantID'=>6,
            'price'=> 4.5,
            'description' => 'Pastis de formatge caser',
            'image'=> 'postre2.jpg',
            'category'=>'Postre',
        ]);

    }
}

class RestaurantsSeeder extends Seeder
{
    /**S
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'name'=> 'Can Xapas',
            'image'=> 'canxapes.png',
            'location'=> 'Carrer València, 145, Barcelona',
            'type'=> 'Mexica',
            'menu'=> 1,
            'discount'=> 1,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'Casa Manoli',
            'image'=> 'canamanoli.jpg',
            'location'=> 'Carrer Tarradelles, 14, Barcelona',
            'type'=> 'Vegetarià',
            'menu'=> 1,
            'discount'=> 0,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'McDonalds',
            'image'=> 'magdonalds.png',
            'location'=> 'Carrer Collbanc, 234, Barcelona',
            'type'=> 'Americà',
            'menu'=> 1,
            'discount'=> 1,
            'lots'=> 0,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'AmeRestaurant',
            'image'=> 'ame.jpg',
            'location'=> 'Avinguda Diagonal, 321, Barcelona',
            'type'=> 'Mexica',
            'menu'=> 0,
            'discount'=> 0,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'Timesburg',
            'image'=> 'timeburg.jpg',
            'location'=> 'Avinguda Diagonal, 301, Barcelona',
            'type'=> 'Americà',
            'menu'=> 0,
            'discount'=> 0,
            'lots'=> 0,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'Can Pizza',
            'image'=> 'canpizza.jpg',
            'location'=> 'Ubicat al carrer València, nº 177',
            'type'=> 'Italià',
            'menu'=> 1,
            'discount'=> 0,
            'lots'=> 1,
        ]);
    }
}


