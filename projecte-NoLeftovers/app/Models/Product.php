<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    

    public function scopeAll($query,$idres) {
        return $query->where('restaurantID','=',$idres)->orderBy('category');
    }

    public function scopeCategory($query,$cat,$idres) {
        return $query->where('restaurantID','=',$idres)->whereIn('category',$cat)->orderBy('category');
    }

    public function scopeProductsid($query,$id) {
        return $query->whereIn('id',$id);
    }

    public function scopeGetcategorys($query,$id){
        return $query->select('category')->where('restaurantID','=',$id)->distinct();
    }

    
}
