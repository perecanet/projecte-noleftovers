<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{

    public function scopeDescomptes() {
        return $this->where('discount','=','1');
    }
    public function scopeMenus() {
        return $this->where('menu','=','1');
    }
    public function scopeLotes() {
        return $this->where('lots','=','1');
    }
    public function scopeTipus($query,$tipus) {
        return $query->where('type','=',$tipus);
    }
    public function scopeSearch($query,$search){

        $query1 = $query->where('name','LIKE','%'.$search.'%')->orWhere('type','LIKE','%'.$search.'%')->orderBy('name');

        return $query1;
    }

    public function scopeGettipos($query){
        return $query->select('type')->distinct();
    }

    public function scopeAll($query){
        $querry1 = $query->where('name','LIKE','%');
        return $querry1;
    }

    public function scopeRestaurantsid($query,$cat) {
        return $query->whereIn('id',$cat);
    }

    public function scopeRestaurantid($query,$id) {
        return $query->where('id',$id);
    }

}
