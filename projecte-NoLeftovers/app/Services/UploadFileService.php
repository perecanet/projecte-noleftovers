<?php

namespace App\Services;

use App\Exceptions\UploadFileException;
use App\Exceptions\PriceTypeException;

class UploadFileService
{

    function uploadFile($file)
    {
        if ($file == null) {
            throw new UploadFileException();
        }
        $fileName = time().$file->getClientOriginalName();
        $file->move(public_path().'/images/', $fileName);
    }

    function checkPrice($price)
    {
        if (!is_numeric($price)) {
            throw new PriceTypeException();
        }
    }
}
