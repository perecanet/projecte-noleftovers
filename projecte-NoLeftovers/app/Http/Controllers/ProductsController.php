<?php

namespace App\Http\Controllers;

use App\Exceptions\PriceTypeException;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\UploadFileService;
use App\Exceptions\UploadFileException;
use App\Models\Restaurant;

use App\Http\Requests\ProductListRequest;

class ProductsController extends Controller
{

    private $product;
    private $uploadService;
    private $error = '';
    private $priceService;
    private $restaurant;

    public function __construct()
    {
        $this->product = new Product();
        $this->restaurant = new Restaurant();
    }

    public function list(Request $request){

        $request->session()->put('restID',$request->input('restaurantID'));
        
        $productes = $this->product->query();
        $productes->all($request->input('restaurantID'));

        $categorys = $this->product->query();
        $categorys->getcategorys($request->input('restaurantID'));

        $restaurantinfo = $this->restaurant->query();
        $restaurantinfo->restaurantid($request->input('restaurantID'));
        
        return view('restaurant')->with('productes', $productes->get())->with('categorys', $categorys->get())->with('restaurant', $restaurantinfo->get());
    }

    public function filter( Request $request){
        $products = $this->product->query(); 
        $restaurantID = session('restID');

        $products->category($request->category,$restaurantID);
        
        return view('restaurant')->with('productes', $products->get());
    }

    public function addToChart(Request $request)
    {
        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, $request->input('productid'));
        $request->session()->put('carrito', $carrito);
        
        return redirect(url()->previous());
    }

    public function borrarChart(Request $request)
    {
        $request->session()->put('carrito', []);
        return redirect(url()->previous());
    }

    public function addProduct(){
        return view('newProduct');
    }
    
    public function storeProduct(Request $request, UploadFileService $UploadFileService)
    {
        $success = false;
        $request->validate([
            'name' => 'required|unique:products',
            'price' => 'required',
            'category' => 'required',
            'description' => 'required',
            'restaurantID',
        ]);
        try {
            
            $this->uploadService = $UploadFileService;
            $this->uploadService->uploadFile($request->file('image'));
            
            $this->priceService = $UploadFileService;
            $this->priceService->checkPrice($request->input('price'));

            $product = new Product;
            $product->name = $request->input('name');
            $product->price  = $request->input('price');
            $product->restaurantID  = 6;
            $product->category  = $request->input('category');
            $product->description  = $request->input('description');
            $product->image  = time().$request->image->getClientOriginalName();
            $success = $product->save();

        } catch (UploadFileException $exception) {
            $this->error = $exception->customMessage();
        } catch (PriceTypeException $exception) {
            $this->error = $exception->customMessage();
        } catch ( \Illuminate\Database\QueryException $exception) {
            $this->error = "Ups.. Hi ha hagut un error amb les dades introduïdes";
        }
        return redirect()->action([ProductsController::class, 'addProduct'], ['success' => $success])->withError($this->error);

    }
}
