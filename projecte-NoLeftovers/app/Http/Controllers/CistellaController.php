<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class CistellaController extends Controller
{

    public function __construct()
    {
        $this->product = new Product();
    }

    /*public $cistellaModel;
    public function __construct() {
        $this->cistellaModel = new Cistella();
    }*/
    public function show(){
        $products = $this->product->query(); 

        $cistella = session('carrito',[]);

        sort($cistella);

        $products->productsid($cistella);


        return view('cistella')->with('productes', $products->get());
    }

    public function deletefromChart(Request $request){
        $cistella = session('carrito',[]);

        $count = 0;
        foreach($cistella as $product){
            if($product == $request->input('id')){
                unset($cistella[$count]);
            }
            $count++;
        }
        $request->session()->put('carrito', $cistella);
        return redirect(url()->previous());

    }
}