<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;




class RestaurantsController extends Controller
{
    public $restaurantsModel;
    public function __construct() {
        $this->restaurantsModel = new Restaurant();
    }
    public function list(){
        $tipus = $this->restaurantsModel->query();
        $tipus->gettipos();

        $restaurants = $this->restaurantsModel->query();
        $restaurants->all();

        return view('restaurants')->with('restaurants',$restaurants->get())->with('categorys',$tipus->get());
    }
    public function listSearch(Request $request){
        $restaurants = $this->restaurantsModel->query();
        $restaurants->search($request->input('search'));

        return view('restaurants')->with('restaurants',$restaurants->get());
    }
    public function listTipus(Request $request){
        $restaurants = $this->restaurantsModel->query();
        $restaurants->tipus($request->input('type'));

        $tipus = $this->restaurantsModel->query();
        $tipus->gettipos();
        return view('restaurants')->with('restaurants',$restaurants->get())->with('categorys',$tipus->get());
    }
    public function listMenus(){
        $restaurants = $this->restaurantsModel->query();
        $restaurants->menus();

        $tipus = $this->restaurantsModel->query();
        $tipus->gettipos();

        return view('restaurants')->with('restaurants',$restaurants->get())->with('categorys',$tipus->get());
    }
    public function listLotes(){
        $restaurants = $this->restaurantsModel->query();
        $restaurants->lotes();

        $tipus = $this->restaurantsModel->query();
        $tipus->gettipos();

        return view('restaurants')->with('restaurants',$restaurants->get())->with('categorys',$tipus->get());
    }
    public function listDescomptes(){
        $restaurants = $this->restaurantsModel->query();
        $restaurants->descomptes();

        $tipus = $this->restaurantsModel->query();
        $tipus->gettipos();

        return view('restaurants')->with('restaurants',$restaurants->get())->with('categorys',$tipus->get());
    }
}
