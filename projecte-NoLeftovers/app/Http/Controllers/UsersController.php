<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public $user;

    public function index(){
        return view('registre');
    }
    public function __construct()
    {
        $this->user = new User();
    }

    public function store(Request $request){


        $request->validate([
        'firstname' => 'required|max 20', 
        'lastname' => 'required|max 50',
        'birthday' => 'required',
        'email' => 'required|unique:users',
        'phone' => 'required|unique:users',
        'password' => 'required',
        'repetPassword' => 'required|min 6',
        'temitscondicions' => 'accepted',
        'reciveinformations',
        ]);

        $user = new User();
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->birthday = $request->input('birthday');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->password = $request->input('password');
        $user->reciveinformations = $request->input('');

        $user->save();

    }

}
