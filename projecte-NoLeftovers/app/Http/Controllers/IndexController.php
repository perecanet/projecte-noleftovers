<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Restaurant;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->product = new Product();
        $this->restaurant = new Restaurant();
    }

    public function index(){
        $products = $this->product->query();
        $restaurants = $this->restaurant->query();

        $products->productsid([1,2,3]);
        $restaurants->restaurantsid([2,4,6]);



        return view('index')->with('indexdata', [ 'restaurants' => $restaurants->get(), 'products' => $products->get()]);
    }


}
