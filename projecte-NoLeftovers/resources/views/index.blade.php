@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.layout')


@section('content')
    <!-- End Top Search -->

    <!-- Start Slider -->
    <div class="container">
        <div id="carouselExampleControls" class="carousel slide col-12 " data-ride="carousel">
                <div class="carousel-inner rectangulo">
                    @foreach($indexdata['restaurants'] as $restaurant)
                    @if($loop->first)
                    <div class="carousel-item active">
                    @else
                    <div class="carousel-item">
                    @endif
                    <li class="text-center">
                        <img src="{{ asset('images/' . $restaurant['image']) }}" class="imageCarrousel mt-5" alt="">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="m-b-20"><strong>{{ $restaurant['name']}}</strong></h1>
                                    <p class="m-b-40">{{ $restaurant['location']}}</p>
                                    <p><a class="btn hvr-hover" href="{{ action([App\Http\Controllers\ProductsController::class, 'list'], ['restaurantID'.'='.$restaurant['id']]) }}">Anar ara</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
        </div>
        </div>


    <!-- Start Categories  -->
    <div class="categories-shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Top ventes</h1>
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($indexdata['products'] as $products)
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img  class="img-fluid imegeSizeIndex" src="{{ asset('images/' . $products['image']) }}" alt="" />
                        <a class="btn hvr-hover" href="{{ action([App\Http\Controllers\ProductsController::class, 'list'], ['restaurantID'.'='.$products['restaurantID']]) }}">Demanar aqui</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!-- End Products  -->

    


    <!-- Start Instagram Feed  -->
   
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @endsection