@extends('layouts.layout')
@section('content')
    <!-- Start Slider -->
    <div class="all-title-box" style="background-image: url('./images/portada.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Crear Producte</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Inici</a></li>
                        <li class="breadcrumb-item"><a href="#">Compte</a></li>
                        <li class="breadcrumb-item"><a href="#">Productes</a></li>
                        <li class="breadcrumb-item active">Crear Producte</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Slider -->
	<div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-3 col text-center">
                    <div class="title-left text-center">
                        <h3>Nou Producte</h3>
                    </div>
                    @if(app('request')->has('success'))
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @else
                    <div class="alert alert-success">
                        El producte s'ha afegit exitosament!
                    </div>
                    @endif
                    @endif
                    <form role="form" method="POST" action="{{route('product.store')}}" class="mt-3 review-form-box" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col col-8">
                                <div class="row mt-4 review-form-box">
                                    <div class="col  col-md-2 col-lg-2">
                                        <label for="name" class="mb-0">Nom:</label>
                                    </div>
                                    <div class="col  col-md-8 col-lg-8">
                                        <input type="text" class="form-control" name="name" placeholder="Nom del producte">
                                    </div>
                                </div>
                                <div class="row mt-4 review-form-box">
                                    <div class="col  col-md-2 col-lg-2 ">
                                        <label for="price" class="mb-0">Preu</label>
                                    </div>
                                    <div class="col col-md-2 col-lg-2">
                                        <input type="text" class="form-control bordegraysolid" name="price" placeholder="00,00">
                                    </div>
                                    <div class="col  col-md-2 col-lg-2 offset-2">
                                        <label for="descripcionProducto" class="mb-0">Categoria</label>
                                    </div>
                                    <div class="col col-md2 col-lg-2">
                                        <input type="text" class="form-control bordegraysolid" name="category" list="category">
                                        <datalist id="categories">
                                            <option value="Beguda">
                                            <option value="Primer">
                                            <option value="Postre">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row mt-4 review-form-box">
                                    <div class="col  col-md-2 col-lg-2 ">
                                        <label for="description" class="mb-0">Descripció</label>
                                    </div>
                                    <div class="col col-md-8 col-lg-8">
                                        <textarea class="form-control antiresize bordegraysolid" rows="10" name="description" placeholder="Descripció del producte"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-3 offset-1 perfil">
                                <div class="row">
                                    <div class="col col-12 col-sm-12 col-md-10 col-lg-9 col-xl-7 offset-3">
                                        <img src="images/perfilcomida.png" class="img-fluid perfilcomida borderSolid">
                                    </div>
                                </div>
                                <br >
                                <div>
                                    <input type="file" name="image" id="image" />
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col col-2 offset-8">
                                <button type="submit" class="btn hvr-hover btn-default">Afegir</button>
                            </div>
                            <div class="col col-2 ">
                                <a href="{{url()->previous()}}" class="btn hvr-hover btn-default">Cancel·lar</a>
                            </div>
                        </div>
                    </form>
                    <br> 
                </div>
            </div>
        </div>
    </div>
@endsection