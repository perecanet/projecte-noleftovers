@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.layout')
@section('content')
@if(!Request::ajax())
<div class="all-title-box" style="background-image: url('./images/portada.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Cistella</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Botiga</a></li>
                    <li class="breadcrumb-item active">Cistella</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container" id="cistella">
    @endif
    @if(count($productes) == 0)
                 <p> La Cistella esta buida </p>
                @else
                
                @php 
                    $preciototal = 0;
                @endphp
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Producte</th>
                                <th>Preu</th>
                                <th>Quantitat</th>
                                <th>Total</th>
                                <th>Esborra</th>
                            </tr>
                        </thead>
                        <tbody >
                        @foreach($productes as $product)
                            <tr>
                                <td class="thumbnail-img">
                                    <a href="#">
                                <img class="img-fluid" src="{{ asset('images/' . $product['image']) }}" alt="" />
                            </a>
                                </td>
                                <td class="name-pr">
                                    <a href="#">
                                    {{ $product['name'] }}
                            </a>
                                </td>
                                <td class="price-pr">
                                    <p>{{ $product['price'] }} €</p>
                                </td>
                                @php
                                    $count = 0;
                                @endphp

                                @forelse(app('request')->session()->get('carrito',[]) as $carritoid)

                                    @if($carritoid == $product['id'])

                                        @php
                                            $count++;
                                            $preciototal = $preciototal + $product['price'];
                                        @endphp

                                    @endif
                                @empty

                                @endforelse

                                <td class="quantity-box"><p size="4" class="c-input-text qty text">{{ $count }}</p>
                                <td class="total-pr">
                                    <p>{{ $product['price'] * $count }} €</p>
                                </td>
                                <td class="remove-pr">
                                    <a href="{{ action([App\Http\Controllers\CistellaController::class, 'deletefromChart'], ['id'.'='.$product['id']]) }}">
                                <i class="fas fa-times"></i>
                            </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <form method="post" id="borrarchart">
            {{ csrf_field() }}
            <input type="submit" value="Buidar Cistella" class="btn btn-green-bn">
        </form>

        <div class="row my-5">
            <div class="col-lg-8 col-sm-12"></div>
            <div class="col-lg-4 col-sm-12">
                <div class="order-box">
                    <h3>Comanda Total</h3>
                    <div class="d-flex">
                        <h4>Sub Total</h4>
                        <div class="ml-auto font-weight-bold"> {{ $preciototal }}€ </div>
                    </div>
                    <div class="d-flex">
                        <h4>Descompte</h4>
                        <div class="ml-auto font-weight-bold"> 0€ </div>
                    </div>
                    <hr class="my-1">
                    <div class="d-flex">
                        <h4>IVA</h4>
                        <div class="ml-auto font-weight-bold"> {{$preciototal*0.1}}€ </div>
                    </div>
                    <div class="d-flex">
                        <h4>Despeses d'enviament</h4>
                        <div class="ml-auto font-weight-bold"> 2€ </div>
                    </div>
                    <hr>
                    <div class="d-flex gr-total">
                        <h5> Total </h5>
                        <div class="ml-auto h5"> {{$preciototal*1.1+2}}€ </div>
                    </div>
                    <hr> </div>
            </div>
            <div class="col-12 d-flex shopping-box"><a href="checkout.html" class="ml-auto btn hvr-hover">Finalitza Pagament</a> </div>
        </div>
        @endif
    </div>
</div>
@if(!Request::ajax())
    <script>
        $('#borrarchart').submit(function(e) {
            e.preventDefault()
            var data = $("#borrarchart").serialize()
            axios.post('restaurant/clearChart', data)
                .then(response => {
                    console.log(response)
                    $('#cistella').replaceWith(response.data)
                })
    })
    </script>
    @endif
    <!-- End Cart -->
@endsection