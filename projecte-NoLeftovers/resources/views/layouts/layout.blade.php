<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>NoLeftovers | Menjar a domicili</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    <!-- Site Icons -->
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('images/logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="{{ action([App\Http\Controllers\IndexController::class, 'index']) }}"><img src="{{ asset('images/logo.png') }}" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item "><a class="nav-link" href="{{ action([App\Http\Controllers\IndexController::class, 'index']) }}">Inici</a></li>
                        <li class="dropdown active">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Botiga</a>
                            <ul class="dropdown-menu">
								<li><a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'list']) }}">Restaurants</a></li>
								<li><a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listDescomptes']) }}">Descomptes</a></li>
                                <li><a href="{{ action([App\Http\Controllers\RestaurantsController::class, 'listLotes']) }}">Lots</a></li>
                            </ul>
                        </li>
                        <li class="dropdown active">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Perfil Usuari</a>
                            <ul class="dropdown-menu">
								<li><a href="{{ action([App\Http\Controllers\ProductsController::class, 'addProduct']) }}">Afegir Nou Producte</a></li>
								<li><a href="#">Configuració</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class="dropdown active">
                            <i class="fa fa-shopping-bag"></i>
                            @if(!app('request')->session()->has('carrito'))
							<span class="badge">0</span>
                            @endif
                            @if(app('request')->session()->has('carrito'))
                            <span class="badge">{{ count(app('request')->session()->get('carrito',[])) }}</span>
                            @endif
                            <a href="{{ action([App\Http\Controllers\CistellaController::class, 'show']) }}"><p>Cistella</p></a>
                            
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->
    
    <!-- Start All Title Box -->
                @yield('content')
            </div>
        </div>
    </div>
    <!-- End Shop Page -->

    <!-- Start Footer  -->
    <footer>
        <div class="footer-main">
            <div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="footer-top-box">
							<h3>Xarxes Socials</h3>
							<ul>
                                <br>
                                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                
                            </ul>
						</div>
					</div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Contacta'ns</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Adreça: Avinguda Diagonal, 467 <br>Barcelona,<br> 08028 </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Telèfon: <a href="tel:+34-933499203">+34-933499203</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:noleftoverscontact@gmail.com">noleftoverscontact@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link">
                            <h4>Informació</h4>
                            <ul>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> About Us</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> Customer Service</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> Our Sitemap</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> Terms &amp; Conditions</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> Privacy Policy</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-child" aria-hidden="true"></i> Delivery Information</p>
                                </li>
                            </ul>
                        </div>
                    </div>
				</div>
                <div class="row">
                    
                    
                    
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">All Rights Reserved. &copy; 2018 <a href="#">ThewayShop</a> Design By :
            <a href="{{ asset('https://html.design/">html design</a></p>
    </div>
    <!-- End copyright  -->

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <!-- ALL PLUGINS -->
</body>

</html>