@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.layout')


@section('content')
@if(!Request::ajax())
<div class="all-title-box" style="background-image: url({{ asset('images/portada.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <h2>{{ $restaurant[0]['name'] }}</h2>
                </div>
                <div class="col">
                    <h3 class="descripcio">{{ $restaurant[0]['type'] }}<br>{{ $restaurant[0]['location'] }}</h3>
                </div>
            </div>
        </div>
    </div>
    <br >
    <div class="products-box">
        <div class="container">
        @if(count($categorys) !=0)
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Les nostres categories</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center row">
                        <div class=" filter-button-group">
                            
                            <form id="filterFormRestaurant" role="form" method="post">
                            @foreach($categorys as $categoria)
                            <button data-filter="pizzes">{{ $categoria['category'] }}</button>
                            <input type="checkbox" class="form-check-input" value="{{ $categoria['category'] }}" name="category[]" {{ (old('$categoria["category"]'))?'checked':'' }}>
                            @endforeach
                            <input type="checkbox" class="form-check-input" value="Postre" name="category[]" {{ (old('Postre'))?'checked':'' }}>
                            
                            <button class="btn hvr-hover " type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            {{ csrf_field() }}
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
            @else
            <h3>Aquest restaurant actualment no te productes</h3>
            @endif
                       
                @endif
                <div id="filtres">
                @php 
                    $productType = "0";
                    $productType2 = "1";
                @endphp
                @foreach($productes as $product)
                    @if($loop->first)
                    @elseif(($productType == $productType2) && ($productType != $product['category']))
                        </div>
                    @elseif($productType != $productType2 && $productType != $product['category'] && $productType2 != $product['category'])
                        </div>
                    @endif

                @if($productType != $product['category'])
                
                <div class="row">
                    <div class="title-all text-center marging-esquerra linia-separacio">
                        <h1>{{ $product['category'] }}</h1>
                    </div>
                </div>
                <div class="row special-list marging-esquerra">
                @endif

                    
                
                        <div class="col-lg-3 col-md-6 special-grid best-seller">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <img src="{{ asset('images/' . $product['image']) }}" class="img-fluid sizeRestaurantImage" alt="Image">
                                    <div class="mask-icon">
                                        <li class="productDescription">{{ $product['description'] }}</li>
                                        <form method="post" action={{ action('ProductsController@addToChart') }}>
                                              {{ csrf_field() }}
                                              <input type="hidden" name="productid" value="{{$product->id}}">
                                              <input type="hidden" name="productname" value="{{$product->name}}">
                                              <input type="submit" value="Afegir a la cistella" class="btn btn-green-bn">
                                        </form>
                                    </div>
                                </div>
                                <div class="why-text">
                                    <h4>{{ $product['name'] }}</h4>
                                    <h5> {{ $product['price'] }} €</h5>
                                </div>
                            </div>
                        </div>
                    @php
                        $productType2 = $productType;
                        $productType = $product['category'];
                    @endphp
                
                    

                @endforeach
            </div>
            
        </div>
    </div>

            @if(!Request::ajax())
            

                <script>
        $('#filterFormRestaurant').submit(function(e) {
            e.preventDefault()
            var data = $("#filterFormRestaurant").serialize()
            axios.post('restaurant', data)
                .then(response => {
                    console.log(response)
                    $('#filtres').replaceWith(response.data)
                })
    })
    </script>
    @endif
@endsection