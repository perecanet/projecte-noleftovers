@extends((Request::ajax()) ? 'layouts.ajax' : 'layouts.layout')


@section('content')
@if(!Request::ajax())
<div class="all-title-box" style="background-image: url({{ asset('images/portada.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Els nostres restaurants</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Inici</a></li>
                        <li class="breadcrumb-item active">Botiga</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
    <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
        <div class="product-categori">
                @include('layouts.filter')
       </div>
    </div>
    @endif
    <div id="content" class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
        <div class="right-product-box">
            <div class="product-categorie-box">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                        <div class="row">
                        @foreach($restaurants as $restaurant)
                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                <div class="products-single fix">
                                    <div class="box-img-hover">
                                        <img src="{{ asset('images/' . $restaurant['image']) }}" class="img-fluid sizeImage" alt="Image">
                                        <div class="mask-icon">
                                            <a class="cart" href="{{ action([App\Http\Controllers\ProductsController::class, 'list'], ['restaurantID'.'='.$restaurant['id']]) }}">Veure Restaurant</a>
                                        </div>
                                    </div>
                                    <div class="why-text">
                                        <h4>{{ $restaurant['name'] }}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


