@extends('layouts.app')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box" style="background-image: url({{ asset('images/portadaConf.jpg') }});">
    <div class="container">
        <div class="row">
            <h2>PÀGINA NO DISPONIBLE</h2>
        </div>
    </div>
</div>
<!-- End All Title Box -->
<!-- Start My Account  -->
<div class="my-account-box-main">
    <div class="container">
        <div class="fa-5x">
            <h2>La URL introduïda no existeix!</h2>
            <i class="fas fa-cog fa-spin"></i><i class="fas fa-cog fa-spin"></i><i class="fas fa-cog fa-spin"></i>
            <h2>Potser t'interessa...</h2>
        </div>
        <div class="my-account-page">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="account-box">
                        <div class="service-box">
                            <div class="service-icon">
                                <a href="#"> <i class="fa fa-home"></i> </a>
                            </div>
                            <div class="service-desc">
                                <h4>Inici</h4>
                                <p>Pàgina Principal</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="account-box">
                        <div class="service-box">
                            <div class="service-icon">
                                <a href="#"><i class="fa fa-lock"></i> </a>
                            </div>
                            <div class="service-desc">
                                <h4>Registrar-se</h4>
                                <p>Donar-se d'alta a la nostra web</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="account-box">
                        <div class="service-box">
                            <div class="service-icon">
                                <a href="#"> <i class="fa fa-shopping-basket"></i> </a>
                            </div>
                            <div class="service-desc">
                                <h4>Restaurants</h4>
                                <p>Els establiments de menjar disponibles</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br /><br /><br />
        </div>
    </div>
</div>
<!-- End My Account -->

@endsection