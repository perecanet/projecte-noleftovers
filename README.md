<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Projecte NoLeftovers

### Sobre nosaltres:

- Autors: Gabriel Barber Servera i Pere Canet Pons.
- Estudis: Grau Superior en Desenvolupament d'Aplicacions Web (DAW).
- Curs 2020 - 2021, Institut Escola del Treball, Barcelona.

### Sobre el projecte:

```
Es tracta d'una web de menjar a domicili, en la qual l'usuari té l'opció de registrar-s'hi
com consumidor o bé venedor.

En el cas de consumidor, pot accedir a tots els establiments registrats a la web i fer comandes
de productes en aquests. Només poden fer-se comandes d'un establiment, en cas de voler productes
de dos de diferents, s'hauran de fer dues comandes separades.

Si l'usuari es vol registrar com venedor, haurà de pujar l'establiment del qual disposa i
l'informació necessària sobre aquest. Un cop fet, podrà registrar tots els productes que ofereix,
a més de descomptes.
```

## Les nostres pàgines i les seves funcionalitats

### Inici / Índex:
- Ruta: ``` resources/views/index.blade.php ```
- Contingut: ``` Mostra un carroussel dels restaurants destacats, i els productes més venuts. ```
- Funcionalitats: ``` Permet accedir a cada un dels restaurants destacats. ```

### Restaurants:
- Ruta: ``` resources/views/restaurants.blade.php ```
- Contingut: ``` Mostra tots els restaurants disponibles. ```
- Funcionalitats: ``` Permet fer cerques tant de nom de restaurant com de categoria, utilitzant petició asíncrona. També compta amb diferents filtres, el quals duen a terme peticions síncrones. Per últim, clicant damunt cada restaurant, accedim a la seva pàgina de productes. ```

### Restaurant:
- Ruta: ``` resources/views/restaurant.blade.php ```
- Contingut: ``` Mostra els productes disponibles d'un restaurant en concret. Per falta de temps, només hem creat la pàgina pel restaurant Can Pizza, els demés es troben buits. ```
- Funcionalitats: ``` Permet filtrar els productes per les categories que el restaurant hagi registrat. Aquest filtratge és asíncron i tenim la possibilitat d'escollir més d'una categoria al mateix temps. El resultat es mostra ordenat alfabèticament. Cada producte que seleccionem, s'afegeix automàticament a la nostra cistella, la qual la trobem a la part superior dreta. ```

### Cistella:
- Ruta: ``` resources/views/cistella.blade.php ```
- Contingut: ``` Mostra tots els productes que tenim seleccionats, la quantitat d'ells i el seu preu. A més, es calcula el preu total, aplicant l'IVA correcte i les despeses d'enviament. En cas de no tenir cap producte seleccionat, es mostra un missatge de cistella buida. ```
- Funcionalitats: ``` Permet esborrar productes concrets de la llista de forma síncrona i buidar tota la cistella de forma asíncrona. ```

### Afegir Nou Producte:
- Ruta: ``` resources/views/newProduct.blade.php ```
- Contingut: ``` Mostra un formulari a omplir per poder pujar a la web un nou producte. En cas de fer-ho correctament, aquest producte es mostrarà a la pàgina restaurant. ```
- Funcionalitats: ``` Permet afegir una imatge, la qual es guardarà a la ruta public/images, i, en cas de no omplir-ho tot o fer-ho malament, consta de diferents excepcions i un validate per controlar-ho. ```

## Conclusió
```
Aquest projecte ha estat una bona manera de descobrir i ser conscients de tota la feina que comporta crear una aplicació web.

En primer lloc, hem hagut de fer el procés de desenvolupament de la idea original del projecte, crear els mockups de com volíem que fos el disseny de la web, estructurar tot el contingut, etc.

Aquesta part ha estat relativament senzilla, només hem hagut d'aprendre a treballar amb bootstrap.

Per altra banda, el backend ha estat la part més complicada, ja que hem hagut de fer tot el procés d’aprenentatge del framework Laravel i a mesura que ho fèiem anàvem creant-lo.

Un cop enteses bé les funcionalitats generals del Laravel i el seu ús, aquesta tasca ha estat molt més ràpida i senzilla.
Tot i així hi hem dedicat molt de temps ja que hi ha molts aspectes en els que treballar, com són els controladors, els models, excepcions, serveis, etc.

Per últim, el projecte també ens ha permès aprendre a treballar amb YouTrack, una eina de gestió de projectes imprescindible per poder dur a terme una feina ordenada i estimar millor els temps de cada tasca.

Així doncs, el projecte ens ha agradat molt i hem après coses de cada camp.
L’únic inconvenient és que ens ha faltat temps per poder desenvolupar-lo encara més.

Moltes gràcies.
```
